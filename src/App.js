import React, { useState,useEffect } from 'react';
import './App.css';
import Container from 'react-bootstrap/Container'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import InputGroup from 'react-bootstrap/InputGroup'
import FormControl from 'react-bootstrap/FormControl'
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import API from './Utils/Api';
import BootstrapTable from 'react-bootstrap-table-next';
import Header from './Components/Header';
import Footer from './Components/Footer';
import { usePromiseTracker } from "react-promise-tracker";
import Loader from 'react-loader-spinner';

function App() {


  let vehicles     = [];

  const [searchTerm, setSearchTerm] = React.useState("");
  const [searchCategory, setSearchCategory] = React.useState("");
  const [ searchResults, setSearchResults] =  React.useState([]);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    API.post("/api/v1/search_vehicles")
      .then(response => {
        setSearchResults(response.data.data );
        setIsLoading(false);
      })
      .catch(function(error) {
        console.log(error);
      });
  }, []);

  const handleClick=(e)=>{

    if(searchTerm==""){
      //alert("No tiene valor");
    }
    //alert(parseInt(searchCategory));
    if(parseInt(searchCategory)==0){
      //alert("No tiene valor 2");
    }

    setIsLoading(true);
    API.post("/api/v1/search_vehicles",
    {"text":searchTerm, "category":searchCategory })
    .then(response => {
      console.log(response.data.data);
      setSearchResults(response.data.data);
      setIsLoading(false);

    }).catch(error => {
            return error;
    });
    

  }

  const columns = [{
    dataField: 'attributes.model',
    text: 'Model'
  }, {
    dataField: 'attributes.brand',
    text: 'Brand'
  }, {
    dataField: 'attributes.price',
    text: 'Product Price'
  },
  , {
    dataField: 'attributes.year',
    text: 'Year'
  }
  , {
    dataField: 'attributes.mileage',
    text: 'Milage'
  }];

  const handleChange = event => {
    setSearchTerm(event.target.value);
  };
  
  const handleChangeSelect = event => {
    setSearchCategory(event.target.value);
  };

  return (



    <Container>
            <Header></Header>
            <span></span>
      <Row>
        <Col sm={4}>
        <InputGroup className="mb-3">
            <InputGroup.Text>Text</InputGroup.Text>
             <FormControl 
                aria-label="Text" 
                placeholder="Search"
                value={searchTerm}
                onChange={handleChange}
              />
            </InputGroup>
        </Col>
        <Col sm={4}>
        <Form.Select onChange={handleChangeSelect}  aria-label="Default select example">
            <option>Open this select menu</option>
            <option value="1">Model Name</option>
            <option value="2">Brand Name</option>
            <option value="3">Year</option>
            <option value="4">Price</option>
            <option value="5">Milage</option>
        </Form.Select>
        </Col>
        <Col sm={4}>
        <Button onClick={handleClick} variant="primary">Search</Button>
        </Col>
      </Row>
      {isLoading ? (
        <p>Loading ...</p>
      ):(
      <Row>
        
        <Col sm><BootstrapTable keyField='id' data={ searchResults } columns={ columns } /></Col>

      </Row>)}
      <Footer></Footer>
    </Container>
  );
}

export default App;
